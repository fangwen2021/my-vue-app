const path = require('path')
import { defineConfig, splitVendorChunkPlugin } from 'vite'
import vue from '@vitejs/plugin-vue'
import legacy from '@vitejs/plugin-legacy'

// https://vitejs.dev/config/
export default defineConfig({
  build: { assetsPublicPath: './' },
  //build: {
  // lib: {
  //   entry: path.resolve(__dirname, 'src/lib/lib.js'),
  //   name: 'MyLib',
  //   // the proper extensions will be added
  //   fileName: 'my-lib'
  // },
  // rollupOptions: {
  //   external: ['vue'],
  //   output: {
  //     // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
  //     globals: {
  //       vue: 'Vue'
  //     }
  //   },
  //   // input: {
  //   //   main: path.resolve(__dirname, 'index.html'),
  //   //   nested: path.resolve(__dirname, 'src/nested/nested.html')
  //   // }
  //   // https://rollupjs.org/guide/en/#big-list-of-options
  // }
  //},
  base: "",
  // experimental: {
  //   //hostType 'js' | 'css' | 'html'
  //   //type 'public' | 'asset'
  //   renderBuiltUrl(filename, { hostType, type }) {
  //     debugger;
  //     console.log(type, 'type000')
  //     if (type === 'asset') {
  //       return 'https://www.domain.com/' + filename
  //     }

  //     else if (path.extname(importer) === '.js') {
  //       return { runtime: `window.__assetsPath(${JSON.stringify(filename)})` }
  //     }
  //     else {
  //       return 'https://cdn.domain.com/assets/' + filename
  //     }
  //   }
  // },
  plugins: [
    vue(),
    splitVendorChunkPlugin(),
    legacy({
      targets: ['defaults', 'not IE 11'],
      //polyfills: ['es.promise.finally', 'es/map', 'es/set'],
      //modernPolyfills: ['es.promise.finally']
    })]
})
